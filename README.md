# RPG HEROES 
**_THE ADVENTURE OF THE KIND HEROES_** is a C# Console Application where you can create heroes and equip them with weapon and armors. 

## Game Logic
* All heroes have Health, Strength, Dexterity and Intellience. 
* Heroes LevelUp by gaining XP and can only equip items that are on the same level or lower 
* Heroes has three slots to store armor in: Head, Body and Legs
    * There are three diffrent armor types with different attributes, plate, cloth and leather.
* Heroes can equip one weapon at a time. Upon a battle (attack) base damage fom the weapon is scaled up depending on level and weapon type.
    * There are three diffrent weapon types with different attributes, ranged weapon, melee weapon and magic weapon.

#### Armor & Weapon:            
    * Armor 
             
             * Can apply bonuses to Health, Strength Dexterity and intelligence
             * Cloth Amor hase a base bonus of 10 health, 3 intelligence, 1 dexterity
             * Scales with 5 Health, 2 Intelligence and 1 Dexterity per Level up
             * Level One has stats: 15 Health, 5 Intelligence, 2 Dexterity

             * Leather Amor hase a base bonus of 20 health, 3 dexterity and 1 strength
             * Scales with 8 Health 2 Dexterity and 1 strength per Level up
             * Level One has stats: 28 Health, 5 Dexterity, 2 Strength
             
             * Plate Amor hase a base bonus of 30 health, 3 Strength, 1 dexterity
             * Scales with 12 Health, 2 Strength and 1 Dexterity per Level up
             * Level One has stats: 42 Health, 5 Strength, 2 Dexterity

    * Weapons Base damage is increased by 
             * Dexterity for ranger weapon
             * Strength for Melee weapon and intelligence for magic weapon 
             * This is all the stats weapon have 
             * Base damage is scaled based on what kind of weapon it is 
             

## Getting Started
* Download or Clone to a local directory
* Open solution in Visual Studio
* Run or F5 to start Console
