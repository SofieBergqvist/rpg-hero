﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    public class Warrior : Hero
    {
        //Constructor - Inherit base class initial name value
        //Set base stats for Warrior
        public Warrior(string name) : base(name)
        {
            HeroType = HeroEnum.Warrior;

            HeroBaseStats = new BaseStats()
            {
                Health = 150,
                Strength = 10,
                Dexterity = 3,
                Intelligence = 1,
        };

            //Effective stats to calculate total stats on attack damage 
            HeroEffectiveStats = new BaseStats()
            {
                Health = HeroBaseStats.Health,
                Strength = HeroBaseStats.Strength,
                Dexterity = HeroBaseStats.Dexterity,
                Intelligence = HeroBaseStats.Intelligence
            };
        }

        //Increase Basestats on LevelUp
        protected override void LevelUp()
        {
            HeroBaseStats.Health += 30;
            HeroBaseStats.Strength += 5;
            HeroBaseStats.Dexterity += 2;
            HeroBaseStats.Intelligence += 1;
        }
    }
}
