﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    //Derived from Hero 
    public class Ranger : Hero
    {
        //Constructor - Inherit from base class
        //Set base stats for Ranger
        public Ranger(string name) : base(name)
        {
            HeroType = HeroEnum.Ranger;

            HeroBaseStats = new BaseStats()
            {
                Health = 120,
                Strength = 5,
                Dexterity = 10,
                Intelligence = 2,
            };

            //Effective stats to calculate total stats on attack damage 
            HeroEffectiveStats = new BaseStats()
            {
                Health = HeroBaseStats.Health,
                Strength = HeroBaseStats.Strength,
                Dexterity = HeroBaseStats.Dexterity,
                Intelligence = HeroBaseStats.Intelligence
            };
        }

        //Increase Basestats on LevelUp
        protected override void LevelUp()
        {
            HeroBaseStats.Health += 20;
            HeroBaseStats.Strength += 2;
            HeroBaseStats.Dexterity += 5;
            HeroBaseStats.Intelligence += 1;
        }
    }
}
