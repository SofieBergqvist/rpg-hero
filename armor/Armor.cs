﻿using RPGHero.hero;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.armor
{

    //Armor base class
    public abstract class Armor
    {
        //Properties
        public string Name { get; set; }
        public int Level { get; set; }
        public BaseStats ArmorBaseStats { get; set; }
        public BaseStats ArmorBonusStats { get; set; }
        public ArmorSlotEnum ArmorSlot { get; set; }
        public ArmorTypeEnum ArmorTypeEnum { get; set; }

        //Protected field
        protected double scaling;

        //Constructor - set base values
        public Armor(string name, int level, ArmorSlotEnum armorSlot, BaseStats armorBaseStats)
        {
            ArmorTypeEnum = ArmorTypeEnum.Undefined;
            Name = name;
            Level = level;
            ArmorSlot = armorSlot;
            ArmorBaseStats = armorBaseStats;

            //Calculate percentages for armor scaling based on values in ArmorSlotEnum class 
            //Numbers are rounded down to integers
            scaling = (int)armorSlot;  
            scaling /= 100; 
        }

        //Display Armor details
        public override string ToString()
        {
            return ($"\nArmor Details for: {Name}" +
                $"\nArmor Type: {ArmorTypeEnum}" +
                $"\nSlot: {ArmorSlot}" +
                $"\nHealth: {ArmorBonusStats.Health}" +
                $"\nDexterity: {ArmorBonusStats.Dexterity}" +
                $"\nIntelligence: {ArmorBonusStats.Intelligence}" +
                $"\nLevel: {Level}");
        }
    }
}
