﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    //Derived from Hero 
    public class Mage : Hero
    {

        //Constructor - Inherit base class initial values
        //Set base stats for Mage
        public Mage(string name) : base(name)
        {
            HeroType = HeroEnum.Mage;

            HeroBaseStats = new BaseStats()
            {
                Health = 100,
                Strength = 2,
                Dexterity = 3,
                Intelligence = 10,
            };

            //Effective stats to calculate total stats on attack damage 
            HeroEffectiveStats = new BaseStats()
            {
                Health = HeroBaseStats.Health,
                Strength = HeroBaseStats.Strength,
                Dexterity = HeroBaseStats.Dexterity,
                Intelligence = HeroBaseStats.Intelligence
            };
        }

        //Increase Basestats on LevelUp
        protected override void LevelUp()
        {
            HeroBaseStats.Health += 15;
            HeroBaseStats.Strength += 1;
            HeroBaseStats.Dexterity += 2;
            HeroBaseStats.Intelligence += 5;
        }

    }
}
