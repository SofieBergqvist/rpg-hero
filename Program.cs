﻿using RPGHero.armor;
using RPGHero.hero;
using RPGHero.weapons;
using RPGHero.Weapons;
using System;
using System.Collections.Generic;

namespace RPGHero
{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("\n\t\tTHE ADVENTURE OF THE KIND HEROES");
            Console.WriteLine("\t_________________________________________________\n\n");

            Console.WriteLine("\n WARRIOR");
            Console.WriteLine("________________");

            //Create Warrior, initialise name
            Hero warrior = new Warrior("Sofie");   

            //Create Weapon, initialise name and level
            Weapon magic = new MagicWeapon("Magic spell", 6);

            //Create Armor, initialise name, level, armor slot and base stats
            Armor plateArmor = new PlateArmor("The Armor of Plate", 15, ArmorSlotEnum.Body, new BaseStats());
            Armor clothArmor = new ClothArmor("Cloth of Destiny", 10, ArmorSlotEnum.Head, new BaseStats());
            Armor leatherArmor = new LeatherArmor("Black Panther Leather loots", 14, ArmorSlotEnum.Legs, new BaseStats());

            Armor blackBite = new ClothArmor("Little black bites", 48, ArmorSlotEnum.Body, new BaseStats());

            //Display base Warrior stats
            Console.WriteLine(warrior.DisplayHeroStats());

            //No attack damage is dealt when weapon slot is empty
            warrior.Attack(magic);


            //Warrior gain XP -> Level up and update base stats
            //Level 1 -> Level 2 needs 100XP, and thereafter +10% each level 
            warrior.GainXP(100);

            //Warrior Equip magic weapon and display stats -> 
            //Weapon Level is higher then Hero Level so this weapon can not be equiped yet
            warrior.EquipWeapon(magic);

            //Warrior gain XP -> Level up and update base stats
            warrior.GainXP(2600);

            //Display base Warrior stats
            Console.WriteLine(warrior.DisplayHeroStats());

            //Warrior Equip magic weapon and display stats
            warrior.EquipWeapon(magic);
            Console.WriteLine(magic.DisplayWeaponStats());


            //Warrior Attack with magic weapon
            //Increase Intelligence 
            warrior.Attack(magic);


            //Warrior Equip Armor and display stats - 
            //Level is to low - will not eqiup weapon
            warrior.EquipArmor(plateArmor);


            warrior.EquipArmor(clothArmor);
            Console.WriteLine(clothArmor.ToString());

            //Warrior gain XP -> Level up and update base stats
            warrior.GainXP(800);

            warrior.EquipArmor(leatherArmor); 
            Console.WriteLine(leatherArmor.ToString());

            //Warrior Attack with magic weapon
            //Increase Intelligence
            warrior.Attack(magic);
            Console.WriteLine(magic.DisplayWeaponStats());

            warrior.RemoveArmor(clothArmor);


            //Warrior Equip new armor for body and display stats
            warrior.EquipArmor(blackBite);
            Console.WriteLine(blackBite.ToString());

            //Warrior Attack with magic weapon
            warrior.Attack(magic);
            Console.WriteLine(magic.DisplayWeaponStats());

            //Display hero stats
            Console.WriteLine(warrior.DisplayHeroStats());


            Console.WriteLine("___________________________________________________________\n");
            Console.WriteLine("\n MAGE");
            Console.WriteLine("________________");
            //Same procedure different hero, equitpment and more Attack

            Hero mage = new Mage("Hana");
            Weapon hug = new MeleeWeapon("Snugelly Hug", 2);
            Armor metalClaw = new PlateArmor("Metal claw ", 2, ArmorSlotEnum.Legs, new BaseStats());
            Armor sneakySilk = new ClothArmor("Sneaky Silk snatch", 6, ArmorSlotEnum.Body, new BaseStats());
            Armor otterLap = new LeatherArmor("Otterlap head patch", 7, ArmorSlotEnum.Head, new BaseStats());

            mage.GainXP(390);
            Console.WriteLine(mage.DisplayHeroStats());


            //Weapon
            mage.EquipWeapon(hug);
            Console.WriteLine(hug.DisplayWeaponStats());

            //Armor
            mage.EquipArmor(metalClaw);
            Console.WriteLine(metalClaw.ToString());

            //Increase Strength 
            mage.Attack(hug);

            //Armor
            mage.EquipArmor(sneakySilk);
            Console.WriteLine(sneakySilk.ToString());

            mage.GainXP(890);


            //Attack
            mage.Attack(hug);

            //Armor
            mage.EquipArmor(sneakySilk);
            Console.WriteLine(sneakySilk.ToString());


            //Armor
            mage.EquipArmor(otterLap);
            Console.WriteLine(otterLap.ToString());


            //Attack
            mage.Attack(hug);
            mage.RemoveArmor(metalClaw);


            Console.WriteLine(mage.DisplayHeroStats());


            Console.WriteLine("___________________________________________________________\n");
            Console.WriteLine("\n RANGER");
            Console.WriteLine("________________");
            //Same procedure different hero, equitpment and more LevelUps


            Hero ranger = new Ranger("Kotaro");
            Weapon water = new RangedWeapon("Water hose", 5);

            Armor tinfoil = new PlateArmor("Tin foil defender hat", 4, ArmorSlotEnum.Head, new BaseStats());
            Armor sock = new ClothArmor("Grandma's sock", 10, ArmorSlotEnum.Legs, new BaseStats());
            Armor kotaro = new LeatherArmor("Otterly defensive by nature", 32, ArmorSlotEnum.Body, new BaseStats());

            ranger.GainXP(1200);
            Console.WriteLine(ranger.DisplayHeroStats());


            //Armor
            ranger.EquipArmor(tinfoil);
            Console.WriteLine(tinfoil.ToString());

            //Weapon
            ranger.EquipWeapon(water);
            Console.WriteLine(water.DisplayWeaponStats());

            //Attack
            //Increase Dexterity
            ranger.Attack(water);
            Console.WriteLine(ranger.DisplayHeroStats());

            //Armor
            ranger.EquipArmor(sock);
            Console.WriteLine(sock.ToString());

            ranger.GainXP(460);

            //New Weapon
            ranger.EquipWeapon(hug);
            Console.WriteLine(hug.DisplayWeaponStats());

            //Armor
            ranger.EquipArmor(kotaro);
            Console.WriteLine(kotaro.ToString());

            ranger.GainXP(1260);

            //Attack
            //Increase Strength 
            ranger.Attack(hug);
            Console.WriteLine(ranger.DisplayHeroStats());


            Console.WriteLine("\n___________________________________________________________\n");

            Console.ReadKey();



        }
    }
}
