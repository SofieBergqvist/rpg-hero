﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    //This class holds the base values used in Hero and Armor Class
    public class BaseStats
    {

        //Properties
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }

        //Constructor with initialised values
        public BaseStats()
        {
            Health = 0;
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;
            Level = 1;
            Name = "";
        }
    }


}
