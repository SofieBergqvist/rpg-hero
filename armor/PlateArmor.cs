﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    //Inherit from Armor 
    public class PlateArmor : Armor
    {

        //Constructor - initialise base values
        public PlateArmor(string name, int level, ArmorSlotEnum armorSlot, BaseStats armorBaseStats)
            : base(name, level, armorSlot, armorBaseStats)
        {
            ArmorTypeEnum = ArmorTypeEnum.Plate;
            ArmorBaseStats.Health = 30;
            ArmorBaseStats.Strength = 3;
            ArmorBaseStats.Dexterity = 1;

            //Calculate new basestat based on bonus and scaling 
            //Integers are the scale-value and the 'scaling' is calculated percentages from enum defined in base class.
            ArmorBonusStats = new BaseStats()
            {
                Health = (int)((ArmorBaseStats.Health + 12 * level) * scaling),
                Strength = (int)((ArmorBaseStats.Strength + 2 * level) * scaling),
                Dexterity = (int)((ArmorBaseStats.Dexterity + 1 * level) * scaling),
            };
        }

        //Display Armor details
        public override string ToString()
        {
            return ($"\nArmor Details for: {Name}" +
                $"\nArmor Type: {ArmorTypeEnum}" +
                $"\nSlot: {ArmorSlot}" +
                $"\nHealth: {ArmorBonusStats.Health}" +
                $"\nStrength: {ArmorBonusStats.Strength}" +
                $"\nDexterity: {ArmorBonusStats.Dexterity}" +
                $"\nLevel: {Level}");
        }
    }
}
