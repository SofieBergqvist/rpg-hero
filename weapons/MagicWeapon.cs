﻿using RPGHero.hero;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.Weapons
{
    public class MagicWeapon : Weapon
    {
        //Magic weapon have a baseDamage of 25 and a Scaling of 2. These are used to calculate total WeaponDamaged
        //Magic Weapons will increase Heros Intelligence upon attack - defined in hero class
        public MagicWeapon(string name, int level) : base(name, 25, level)
        {
            WeaponType = WeaponEnum.Magic;
            Scaling = 2;
            Level = level;
            BaseDamage = WeaponDamage(Scaling);
        }
    
    }
}
