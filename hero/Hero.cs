﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    public abstract class Hero
    {
        //Base class properties
        public string Name { get; set; }
        public int Level { get; set; }
        public int CurrentXp { get; set; }
        public int XPToNextLevel { get; set; }
        public int AttackDamage { get; set; }
        public Weapon WeaponType { get; set; }
        public BaseStats HeroBaseStats { get; set; }
        public BaseStats HeroLevelUpStats { get; set; }
        public BaseStats HeroEffectiveStats { get; set; }
        public BaseStats ArmorBonusStats { get; set; }

        //Propertis to check if Armor slots has equipt armour 
        public Armor HeadArmor { get; set; } = null;
        public Armor BodyArmor { get; set; } = null;
        public Armor LegsArmor { get; set; } = null;
        public ArmorTypeEnum ArmorType { get; set; }
        public ArmorSlotEnum ArmorSlot { get; set; }

        public HeroEnum HeroType { get; set; }
        public int CurrentXP { get; set; }

        protected double attackDamage;

        protected bool weaponEquiped = false;
        protected bool armorEquiped = false;



        //Create new Dictionaries to store Armor Equipment and Weapon. 
        protected Dictionary<ArmorSlotEnum, Armor> EquipedArmor { get; set; } = new Dictionary<ArmorSlotEnum, Armor>();
        protected Dictionary<WeaponEnum, Weapon> EquippedWeapon { get; set; } = new Dictionary<WeaponEnum, Weapon>();

        //Constructor - set base values. Every Hero starts at Level 1.
        public Hero(string name)
        {
            HeroType = HeroEnum.Undefined;
            Name = name;
            Level = 1;
            CurrentXP = 0;
            XPToNextLevel = 100;
        }

        //Method for adding XP to our Hero and increase level
        //Increase CurrentXP and increase baseStats from LevelUp method specified in derived classes
        //100XP required from level 1 -> 2, Thereafter a 10% increase for each Level, rounded down to int.
        public virtual void GainXP(int addXp)
        {
            CurrentXP += addXp;

            while (addXp > 0)
            {
                addXp -= XPToNextLevel;

                if (CurrentXP >= XPToNextLevel)
                {
                    LevelUp();
                    Level++;
                    CurrentXP -= XPToNextLevel;
                    XPToNextLevel = (int)(XPToNextLevel * 1.1);
                    HeroEffectiveStats = UpdateEffectiveHeroStats();
                }
            }
        }

        //Adding baseStats on LevelUp - base stats increment in derived classes
        protected abstract void LevelUp();

        //Equip Armor - Check for matching key value and adds Armor to emty slot or replace existing armor
        public void EquipArmor(Armor armor)
        {
            if (Level < armor.Level)
            {
                Console.WriteLine("\nYou can't equip this item yet. Gain more XP!\n");
                return;
            }

            //Equips armor based on wich Armorslot it belongs to
            switch (armor.ArmorSlot)
            {
                case ArmorSlotEnum.Head:
                    HeadArmor = armor;
                    break;
                case ArmorSlotEnum.Body:
                    BodyArmor = armor;
                    break;
                case ArmorSlotEnum.Legs:
                    LegsArmor = armor;
                    break;
                default:
                    break;
            }
            Console.WriteLine("\nArmor Equipped!\n");

            UpdateEffectiveHeroStats();
        }


        public void RemoveArmor(Armor armor)
        {
            HeroEffectiveStats.Health -= armor.ArmorBonusStats.Health;
            HeroEffectiveStats.Health -= armor.ArmorBonusStats.Strength;
            HeroEffectiveStats.Intelligence -= armor.ArmorBonusStats.Intelligence;
            HeroEffectiveStats.Dexterity -= armor.ArmorBonusStats.Dexterity;
            Console.WriteLine("\nArmor removed");
        }


        //Update Hero Effective stats with bonuses
        public BaseStats UpdateEffectiveHeroStats()
        {
            //Set Hero Effective stats to basestas
            HeroEffectiveStats.Health = HeroBaseStats.Health;
            HeroEffectiveStats.Strength = HeroBaseStats.Strength;
            HeroEffectiveStats.Dexterity = HeroBaseStats.Dexterity;
            HeroEffectiveStats.Intelligence = HeroBaseStats.Intelligence;

            if (BodyArmor != null)
            {
                HeroEffectiveStats.Health += BodyArmor.ArmorBonusStats.Health;
                HeroEffectiveStats.Strength += BodyArmor.ArmorBonusStats.Strength;
                HeroEffectiveStats.Dexterity += BodyArmor.ArmorBonusStats.Dexterity;
                HeroEffectiveStats.Intelligence += BodyArmor.ArmorBonusStats.Intelligence;
            }

            if (HeadArmor != null)
            {
                HeroEffectiveStats.Health = HeadArmor.ArmorBonusStats.Health;
                HeroEffectiveStats.Strength += HeadArmor.ArmorBonusStats.Strength;
                HeroEffectiveStats.Dexterity += HeadArmor.ArmorBonusStats.Dexterity;
                HeroEffectiveStats.Intelligence += HeadArmor.ArmorBonusStats.Intelligence;
            }
     
            if (LegsArmor != null)
            {
                HeroEffectiveStats.Health += LegsArmor.ArmorBonusStats.Health;
                HeroEffectiveStats.Strength += LegsArmor.ArmorBonusStats.Strength;
                HeroEffectiveStats.Dexterity += LegsArmor.ArmorBonusStats.Dexterity;
                HeroEffectiveStats.Intelligence += LegsArmor.ArmorBonusStats.Intelligence;
            }
            return HeroEffectiveStats;
        }

        //Equip and add weapon to weapon slot 
        public void EquipWeapon(Weapon weapon)
        {
            if (Level >= weapon.Level)
            {
                Console.WriteLine("\nWeapon Equipped!\n");
                if (EquippedWeapon.ContainsKey(weapon.WeaponType))
                {
                    EquippedWeapon[weapon.WeaponType] = weapon;
                    weaponEquiped = true;

                }
                else
                {
                    EquippedWeapon[weapon.WeaponType] = weapon;
                    weaponEquiped = true;
                }
            }
            else
            {
                Console.WriteLine("\nYou can't equip this weapon yet. Gain more XP!");
                weaponEquiped = false;
            }

        }

        //When Weapon is Equiped -> Calculate attack damage by multiplying hero attribute with constant scale  
        //increase Hero attribute based on weapon type
        public int Attack(Weapon weapon)
        {
            if (weaponEquiped)
            {
                //Switch on Enum weapon type, return when there is a match
                switch (weapon.WeaponType)
                {
                    case WeaponEnum.Melee:
                        attackDamage = HeroEffectiveStats.Strength * (int)1.5;
                        Console.WriteLine($"\nATTACK! BANG!\n");
                        return HeroBaseStats.Strength += (int)attackDamage;
                    case WeaponEnum.Ranged:
                        attackDamage = HeroEffectiveStats.Dexterity * 2;
                        Console.WriteLine($"\nATTACK! LONG SHOT!!\n");
                        return HeroBaseStats.Dexterity += (int)attackDamage;
                    case WeaponEnum.Magic:
                        attackDamage = HeroEffectiveStats.Intelligence * 3;
                        Console.WriteLine($"\nATTACK! MAGIC SPELL!!\n");
                        return HeroBaseStats.Intelligence += (int)attackDamage;
                    default:
                        break;
                }
                return AttackDamage;
            }
            else
            {
                Console.WriteLine("\nYou don't have any weapon yet");

                return 0;
            }
        }

        //Display Hero stats 
        public virtual StringBuilder DisplayHeroStats()
        {
            StringBuilder heroStats = new StringBuilder();

            heroStats.Append($"\n{HeroType} Details").AppendLine();
            heroStats.Append($"Name: {Name}").AppendLine();
            heroStats.Append($"Health: {HeroEffectiveStats.Health}").AppendLine();
            heroStats.Append($"Strength: {HeroEffectiveStats.Strength}").AppendLine();
            heroStats.Append($"Dexterity: {HeroEffectiveStats.Dexterity} ").AppendLine();
            heroStats.Append($"Intelligence: {HeroEffectiveStats.Intelligence}").AppendLine();
            heroStats.Append($"Level: {Level} ").AppendLine();
            heroStats.Append($"XP to next Level: {XPToNextLevel }").AppendLine();
            return heroStats;
        }

    }
}
