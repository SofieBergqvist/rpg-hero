﻿using RPGHero.hero;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.weapons
{
    //Derived from base class Weapon
    public class MeleeWeapon : Weapon
    {

        //Melee weapon have a baseDamage of 15 and a Scaling of 2. These are used to calculate total WeaponDamaged
        //Melee Weapons will increase Heros Strength upon attack - defined in hero class
        public MeleeWeapon(string name, int level) : base(name, 15, level)
        {
            WeaponType = WeaponEnum.Melee;
            Scaling = 2;
            Level = level;
            BaseDamage = WeaponDamage(Scaling);
        }
  
    }
}
