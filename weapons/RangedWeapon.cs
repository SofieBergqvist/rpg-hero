﻿using RPGHero.hero;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.weapons
{
    //Derived from base class Weapon
    public class RangedWeapon : Weapon
    {
        //Ranged weapon have a baseDamage of 5 and a Scaling of 3. These are used to calculate total WeaponDamaged
        //Ranged Weapons will increase Heros Dexterity upon attack - defined in hero class
        public RangedWeapon(string name, int level) : base(name, 5, level)
        {
            WeaponType = WeaponEnum.Ranged;
            Scaling = 3;
            Level = level;
            BaseDamage = WeaponDamage(Scaling);
        }

    }
}
