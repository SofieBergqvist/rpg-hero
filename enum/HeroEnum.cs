﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    /*
     * Container for our Heros 
     * assigned values instead of auto increment
     */
    public enum HeroEnum
    {
        Undefined,
        Warrior, 
        Ranger,
        Mage
    }

}
