﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.armor
{
    public enum ArmorTypeEnum
    {
        Undefined,
        Cloth,
        Plate,
        Leather
    }

}

