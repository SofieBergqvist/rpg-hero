﻿using RPGHero.hero;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.weapons
{
    public abstract class Weapon
    {
        //Properties
        public string Name { get; set; }
        public int BaseDamage { get; set; }
        public int Scaling { get; set; }
        public int Level { get; set; }
        public int AttackDamage { get; set; }
        public WeaponEnum WeaponType { get; set; }
        public BaseStats BaseStats { get; set; }

        //Constructor
        public Weapon(string name, int baseDamage, int level)
        {
            Name = name;
            BaseDamage = baseDamage;
            Scaling = 0;
            Level = level;
        }

        //Calculate Weapon damage upon Attack
        //Each Weapon scales differently by level, scaling value is set in derived class
        protected int WeaponDamage(int Scaling)
        {
            BaseDamage += (Scaling * Level);
            return BaseDamage;
        }

        //Display weapon stats
        public virtual StringBuilder DisplayWeaponStats()
        {
            StringBuilder weaponStats = new StringBuilder();

            weaponStats.Append($"Weapon Details").AppendLine();
            weaponStats.Append($"Name: {Name}").AppendLine();
            weaponStats.Append($"Weapon Type: {WeaponType}").AppendLine();
            weaponStats.Append($"Weapon Level: {Level}").AppendLine();
            weaponStats.Append($"Weapon Damage: {BaseDamage} ").AppendLine();
            return weaponStats;
        }

    }
}
