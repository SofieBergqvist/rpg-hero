﻿using RPGHero.armor;
using RPGHero.weapons;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.hero
{
    //Inherit from Armor 
    public class ClothArmor : Armor
    {

        //Constructor - initialise base values
        public ClothArmor(string name, int level, ArmorSlotEnum armorSlot, BaseStats armorBaseStats)
            : base(name, level, armorSlot, armorBaseStats)
        {
            ArmorTypeEnum = ArmorTypeEnum.Cloth;
            ArmorBaseStats.Health = 10;
            ArmorBaseStats.Dexterity = 1;
            ArmorBaseStats.Intelligence = 3;


            //Calculate new basestat based on bonus and scaling 
            //Integers are the scale-value and the 'scaling' is calculated percentages from enum defined in base class.
            ArmorBonusStats = new BaseStats()
            {
                Health = (int)((ArmorBaseStats.Health + 5 * Level) * scaling),
                Dexterity = (int)((ArmorBaseStats.Dexterity + 1 * Level) * scaling),
                Intelligence = (int)((ArmorBaseStats.Intelligence + 2 * Level) * scaling)
            };
        }

        //Display Armor details
        public override string ToString()
        {
            return ($"\nArmor Details for: {Name}" +
                $"\nArmor Type: {ArmorTypeEnum}" +
                $"\nSlot: {ArmorSlot}" +
                $"\nHealth: {ArmorBonusStats.Health}" +
                $"\nDexterity: {ArmorBonusStats.Dexterity}" +
                $"\nIntelligence: {ArmorBonusStats.Intelligence}" +
                $"\nLevel: {Level}");
        }
    }
}
