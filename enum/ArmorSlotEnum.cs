﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGHero.armor
{
    public enum ArmorSlotEnum
    {
        //Assign default values to scale Armor in percentages
        Body = 100,
        Head = 80,
        Legs = 60,
    }
}
